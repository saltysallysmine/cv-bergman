#let conf(title, doc) = {
  set page(
    header: align(
      right,
      title
    )
  )
  set text(size: 12pt)
  set par(justify: true)
  doc
}

#show: doc => conf(
  [Curriculum Vitae],
  doc,
)

#let delimiter = rect(
  width: 100%,
  height: 1pt,
  inset: 4pt,
  fill: black,
)

#set page(numbering: "— 1 —")

#show link: underline

// Content

#align(center)[
  = Бергман Валерий
]

\

#grid(
  columns: (2fr, 3fr),
  align(center)[
    #image("img/my-photo-rounded.png", width: 90%)
  ],
  align(center)[
    \
    \

    МФТИ Высшая школа программной инженерии, 2026

    *Backend-разработчик, системный программист*

    *Linux C/C++ Go*

    #link("mailto:bergman.vd@phystech.edu") #link("https://gitlab.com/saltysallysmine")[GitLab] #link("https://github.com/saltysallysmine")[GitHub] #link("https://t.me/bergman_vd")[Telegram]

  ],
)

#align(center)[
  == Бэкенд. Go, Java
  #delimiter
]

Эти языки я изучил на совместных курсах МФТИ и МТС, писал учебные проекты,
домашки. Разрабатываю с помощью:

- Стандартной библиотеки Go и фреймворка Go Chi;
- Java Spring.

Знаю юнит- и интеграционное тестирование, логирование, метрики Prometheus,
визуализацию Grafana, контейнеризацию Docker и базы данных: SQL и NoSQL.

Есть опыт написания конкурентного кода, работал с гарантиями доставки сообщений
и консистентностью данных в распределённой среде (Transactional Outbox).

В профиле #link("https://gitlab.com/saltysallysmine")[GitLab] вы можете увидеть
репозитории с моими работами:

- Go - #link("https://gitlab.com/saltysallysmine/mts-teta-go-homeworks")[Mts
Teta Go Homeworks];
- Java - #link("https://gitlab.com/saltysallysmine/mts-teta-java-homeworks")[Mts
Teta Homeworks].

Для тестирования использую Python ввиду его лаконичности и удобства. Поднимаю
тестовые страницы или API-обертки для скриптов с помощью Flask или Bottle.

#align(center)[
  == Системная разработка. Параллельное программирование (C/C++)
  #delimiter
]

Владею командной строкой Linux, регулярно пишу скрипты на Bash для личного
пользования. Увлечён низкоуровневым программированием. В МФТИ изучаю курсы
системной разработки и параллельного программирования.

- *Реализации* `std::ostream`, `std::istream` и `std::filesystem::recursive_directory_iterator`.
  #link("https://gitlab.com/saltysallysmine/hsse-caos-homeworks")[HSSE-CAOS-Homeworks].

- *Задачи на OpenMP, MPI, UNIX Threads.*
  #link("https://gitlab.com/saltysallysmine/hsse-parallel-programming")[HSSE-parallel-programming].

#pagebreak()

#align(center)[
  == Прочее
  #delimiter
]

- *Риторика.* Много раз презентовал свои работы, занимался в театральной
  студии, поэтому имею богатый опыт публичных выступлений.

- *Работа в команде.* Обычно я вовлечён в разные аспекты проекта: могу
  разрабатывать код, общаться с заказчиком, искать новых участников. Принимаю на
  себя организационную работу.

  К примеру, на национальной олимпиаде по анализу данных "DANO" в своей команде
  я был лидером и наладил работу между ребятами, которые никогда раньше не знали
  друг друга. Мы победили в соревновании.

- *Data Science.* Стажировался в МТС на позиции дата сайентиста, имею опыт
  работы со Spark, PyTorch, SciPy. Есть проекты со свёрточными нейронными
  сетями, CycleGAN.

- *Python.* Несколько работ с моего GitHub: нейронная сеть для анализа
  изображений -- учебная реализация; веб-сервисы и Telegram-боты.

- *Flutter.* Прошёл совместный курс МФТИ и Яндекса, учебный проект --
  #link("https://gitlab.com/saltysallysmine/news_api")[новостное приложение].

- *Arduino и Raspberry Pi*. Собирал схемы с датчиками, камерой и сервоприводом.

