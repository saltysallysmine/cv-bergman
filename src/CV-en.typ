#let conf(title, doc) = {
   set page(
     header: align(
       right,
       title
     )
   )
   set text(size: 12pt)
   set par(justify: true)
   doc
}

#show: doc => conf(
   [Curriculum Vitae],
   doc,
)

#let delimiter = rect(
   width: 100%,
   height: 1pt,
   inset: 4pt,
   fill: black,
)

#set page(numbering: "— 1 —")

#show link: underline

// Content

#align(center)[
  = Bergman Valery
]

\

#grid(
   columns: (3fr, 2fr),
   align(center)[
     \
     \

     Moscow Institute of Physics and Technology

     *Backend developer, systems programmer*

     Linux C/C++ Go Java Python

     #link("mailto:bergman@phystech.edu") #link("https://gitlab.com/saltysallysmine")[GitLab] #link("https://github.com/saltysallysmine")[GitHub] #link("https://t.me/bergman_vd")[Telegram]

   ],
   align(center)[
     #image("img/my-photo-rounded.png", width: 90%)
   ],
)

== Backend. Go, Java, Python

I develop web services of varying complexity. I write in Java using Spring and in Go with the standard library and the Chi framework. I know and can use unit and integration testing, logging, Prometheus metrics, Grafana visualization, Docker containerization, and databases (SQL and NoSQL). In addition, I have experience writing concurrent code, and I know how to guarantee message delivery in various ways and maintain data consistency in a distributed environment.

In my #link("https://gitlab.com/saltysallysmine")[GitLab] profile, you can see repositories with my works:

- Java: #link("https://gitlab.com/saltysallysmine/mts-teta-java-homeworks")[Mts Teta Homeworks]\;
- Go: #link("https://gitlab.com/saltysallysmine/mts-teta-go-homeworks")[Mts Teta Go Homeworks].

I actively use Python for testing due to its conciseness and convenience. Sometimes I use it to launch test pages or API wrappers for scripts using Flask or Bottle.

== System development. Linux

I am interested in the Linux system, and I spend a significant part of my time studying it. I am proficient in the command line and regularly write Bash scripts for personal use. I'm also passionate about low-level programming. On my GitLab you can find training implementations of `std::ostream`, `std::istream` and `std::filesystem::recursive_directory_iterator` (the last one is not finished yet).

- #link("https://gitlab.com/saltysallysmine/hsse-caos-homeworks")[HSSE-CAOS-Homeworks]. Works are currently written in C++, but in the future there may be projects written in pure C.

I worked with Arduino and Raspberry Pi, putting together simple circuits with sensors, a camera, and a servo drive.

== Other

- *Python.* I know Python quite well; several works, which are posted on my GitHub, have been written in this language. Among them, a neural network for image analysis is an educational implementation that is not applicable in practice; web services; and Telegram bots. I also have experience working with libraries for data analysis and machine learning, convolutional neural networks, and CycleGAN.
- *Rhetoric.* I have presented my works many times and studied in a theater studio for a long time, so I have a rich experience in public speaking.
- *Teamwork.* I like to work with other people and learn from their experiences. Usually I am involved in different aspects of the project: I can develop code, communicate with the customer, and look for new participants. If necessary, I take on organizational work. For example, at the national data analysis olympiad called "DANO" I was a leader in my team, and I managed to establish work between students who had never known each other before. Together we led the team to victory.
